import java.util.Scanner;

class Program {

	public static void sendEmail(String email) {
		emailService.send(email, "link");
	}

	public static User getDataFromUser() {
		Scanner scanner = new Scanner(System.in);
		String name = scanner.nextLine();
		String email = scanner.nextLine();
		return new User(name, email);
	}

	public static void main(String[] args) {
		User user = getDataFromUser();
		sendEmail(user.getEmail());

	}
}
